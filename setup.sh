#!/bin/sh
APP_NAME=$1
APP_ENGINE_ID=$2
echo "application: $APP_ENGINE_ID
version: 1
runtime: python27
api_version: 1
threadsafe: yes

handlers:
- url: /styles
  static_dir: styles

- url: /js
  static_dir: js

- url: /favicon.ico
  static_files: favicon.ico
  upload: favicon.ico

- url: .*
  script: main.application

libraries:
- name: webapp2
  version: \"2.5.1\"
- name: markupsafe
  version: \"0.15\"
" > app.yaml

echo "#!/bin/sh
python2 web/template.py --compile $APP_NAME/pages/templates/
lessc styles/main.less > styles/main.css
" > compile
chmod +x compile

mkdir js styles $APP_NAME

echo "import logging
from $APP_NAME import config
logging.basicConfig(level=logging.DEBUG if config.DEBUG else logging.INFO)

import webapp2

from $APP_NAME.pages import Index

urls = [ ( '/', Index)
       ,
       ]

application = webapp2.WSGIApplication(urls, debug=config.DEBUG)
" > main.py

touch styles/main.less

cd $APP_NAME

mkdir models pages pages/templates

touch __init__.py models/__init__.py

echo "import webapp2
import web

render = web.template.render(\"$APP_NAME/pages/templates/\", base='base')

class Index(webapp2.RequestHandler):
    def get(self):
        self.response.write(render.index(\"Contrafibulations! You've gotten it working!\"))
" > pages/__init__.py

echo "# [Basics]
DEBUG = True
" > config.py

echo "\$def with(page)
<!doctype html>
<html>
    <head>
        <title>\$page.title</title>
    </head>
    <body>
        \$:page
    </body>
</html>
" > pages/templates/base.html

echo "\$def with(message)
\$var title: It probably works

<h1>\$message</h1>
" > pages/templates/index.html

cd ..
cp /usr/lib/python2.7/site-packages/web . -r
./compile
