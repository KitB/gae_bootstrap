# Google App Engine Bootstrap
I found myself regularly using a very similar basic setup for applications on
Google App Engine so I decided to automate the process of setting such an
application up.

This is fairly specialised to my own use case but might be useful to others so
I thought I'd make it public.

All it does is create a basic file structure and echo contents into some files
then copy web.py over from its default install location on my machine.
